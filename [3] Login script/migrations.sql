/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : examen

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-06-10 22:20:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2019_04_24_095137_create_permission_tables', '1');

-- ----------------------------
-- Table structure for `model_has_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of model_has_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for `model_has_roles`
-- ----------------------------
DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of model_has_roles
-- ----------------------------
INSERT INTO `model_has_roles` VALUES ('1', 'App\\User', '1');
INSERT INTO `model_has_roles` VALUES ('3', 'App\\User', '6');
INSERT INTO `model_has_roles` VALUES ('3', 'App\\User', '7');
INSERT INTO `model_has_roles` VALUES ('3', 'App\\User', '8');
INSERT INTO `model_has_roles` VALUES ('3', 'App\\User', '10');

-- ----------------------------
-- Table structure for `permissions`
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('1', 'edit.user', 'web', '2019-04-26 12:06:20', '2019-04-26 12:06:20');
INSERT INTO `permissions` VALUES ('2', 'delete.user', 'web', '2019-04-26 12:06:20', '2019-04-26 12:06:20');
INSERT INTO `permissions` VALUES ('3', 'activate.user', 'web', '2019-04-26 12:06:20', '2019-04-26 12:06:20');
INSERT INTO `permissions` VALUES ('4', 'create.user', 'web', '2019-04-26 15:45:32', '2019-04-26 15:45:34');
INSERT INTO `permissions` VALUES ('5', 'edit.article', 'web', '2019-04-29 07:44:45', '2019-04-29 07:44:45');
INSERT INTO `permissions` VALUES ('6', 'delete.article', 'web', '2019-04-29 07:44:45', '2019-04-29 07:44:45');
INSERT INTO `permissions` VALUES ('7', 'create.article', 'web', '2019-04-29 07:44:45', '2019-04-29 07:44:45');
INSERT INTO `permissions` VALUES ('8', 'edit.personal.user', 'web', '2019-05-28 11:26:32', '2019-05-28 11:26:32');
INSERT INTO `permissions` VALUES ('9', 'edit.password.user', 'web', '2019-05-28 11:26:32', '2019-05-28 11:26:32');
INSERT INTO `permissions` VALUES ('10', 'edit.roles.user', 'web', '2019-05-28 11:26:32', '2019-05-28 11:26:32');

-- ----------------------------
-- Table structure for `role_has_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of role_has_permissions
-- ----------------------------
INSERT INTO `role_has_permissions` VALUES ('1', '1');
INSERT INTO `role_has_permissions` VALUES ('2', '1');
INSERT INTO `role_has_permissions` VALUES ('3', '1');
INSERT INTO `role_has_permissions` VALUES ('4', '1');
INSERT INTO `role_has_permissions` VALUES ('8', '1');
INSERT INTO `role_has_permissions` VALUES ('9', '1');
INSERT INTO `role_has_permissions` VALUES ('10', '1');

-- ----------------------------
-- Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'administrator', 'web', '2019-04-26 12:06:20', '2019-04-26 12:06:20');
INSERT INTO `roles` VALUES ('3', 'user', 'web', '2019-05-29 09:24:42', '2019-05-29 09:24:42');

-- ----------------------------
-- Table structure for `settings`
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `name` varchar(100) DEFAULT NULL,
  `value` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES ('website.url', 'test');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(1) DEFAULT 0,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `social_facebook` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '#',
  `social_instagram` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '#',
  `social_twitter` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '#',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Administrator', 'Jeroen', 'Demo', '$2y$10$y.0lqUQkISvwvSymvvpJbOOVoq5YXq7wG54rH/iS9eKv0D.hZ.uWa', '1', 'ps159931@fronter.summacollege.nl', '#', '#', '#', 'mtqfwXwlo2OgKa481iqfUKlzxRu2vtgNoOzdxzJVKhA1kopgKxClu5OiRi3V', '2019-06-10 21:54:10', '2019-06-10 21:54:10');
INSERT INTO `users` VALUES ('10', 'User', 'Jeroen', 'Demo', '$2y$10$y.0lqUQkISvwvSymvvpJbOOVoq5YXq7wG54rH/iS9eKv0D.hZ.uWa', '1', 'ps159931@fronter.summacollege.nl', '#', '#', '#', '3Bm2lmwo4kHM5QTLCybxF7xa3EKGGt7kLC19iDiPDM2ZFxQYPTHWF946hlvu', '2019-06-10 21:29:39', '2019-06-10 21:29:39');
