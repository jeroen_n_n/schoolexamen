<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get(null, 'Auth\LoginController@show')->name('login');
Route::post(null, 'Auth\LoginController@login')->name('login');
    
Route::group(['middleware' => 'auth'], function() {
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');

    Route::group(['prefix' => 'settings', 'as' => 'settings.'], function() {
        Route::get(null, 'SettingsController@index')->name('index');
        Route::put('personal', 'SettingsController@update_personal')->name('personal');
        Route::put('password', 'SettingsController@update_password')->name('password');
    });

    Route::group(['prefix' => 'user', 'as' => 'user.'], function() {
        Route::get(null, 'UserController@index')->name('index');
        
        Route::get('/create', 'UserController@create')->name('create');
        Route::post('/store', 'UserController@store')->name('store');
        
        Route::get('/{id}', 'UserController@show')->name('show');
        Route::delete('/{id}', 'UserController@destroy')->name('destroy');

        Route::get('/{id}/edit', 'UserController@edit')->name('edit');

        Route::put('/{id}/update', 'UserController@update')->name('update');
        Route::put('/{id}/active/{action}', 'UserController@active')->name('active');
    });
});