@extends('parts.header')

@section('content')
    <div class="row">
        <div class="col">
            @if($errors->first())
                <div class="alert alert-icon alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert"></button>
                    <i class="fe fe-alert-triangle mr-2" aria-hidden="true"></i> {{ $errors->first() }}
                </div>
            @endif

            @if (Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert"></button>
                    {{ Session::get('success') }}
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Overzicht gebruikers ({{ $users->total() }})</h3>

                    @can('create.user')
                        <div class="card-options">
                            <a href="{{ route('user.create') }}" class="btn btn-secondary"><i class="fe fe-plus-circle"></i> Maak een account</a>
                        </div>
                    @endcan
                </div>

                <div class="table-responsive">
                    <table class="table table-hover table-outline table-vcenter text-nowrap card-table" width="100%">
                        <thead>
                            <tr>
                                <th class="text-center w-1"></th>
                                <th>{{ trans('attribute.user.name') }}</th>
                                <th>{{ trans('attribute.user.email') }}</th>
                                <th >{{ trans('attribute.user.rank') }}</th>
                                <th class="text-center" width="60"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td class="text-center">
                                         <span class="avatar @if($user->active) avatar-green @else avatar-red @endif">
                                            {{ mb_substr($user->firstname, 0, 1, "UTF-8") .  mb_substr($user->lastname, 0, 1, "UTF-8") }}
                                        </span>
                                    </td>
                                    
                                    <td>
                                        <div>{{ $user->name }}</div>
                                        <div class="small text-muted">
                                            {{ $user->firstname }} {{ $user->lastname }}
                                        </div>
                                    </td>

                                    <td>
                                        {{ $user->email }}
                                    </td>
                                    
                                    <td>
                                        {{ ucfirst( isset($user->roles[0]) ? $user->roles[0]->name : 'User') }}
                                    </td>
                                    
                                    <td>                                      
                                        @can('edit.user')
                                            <a href="{{ route('user.edit', $user->id) }}" class="btn btn-secondary"><i class="fe fe-edit-2" data-toggle="tooltip" data-original-title="Bewerk profiel"></i></a>
                                        @endcan

                                        <a href="{{ route('user.show', $user->id) }}" class="btn btn-primary"><i class="fe fe-user" data-toggle="tooltip" data-original-title="Bekijk gebruiker"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="modal-footer">
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection