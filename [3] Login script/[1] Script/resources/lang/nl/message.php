<?php

return [
    'settings' => [
        'success'                       =>      'Instellingen succesvol aangepast.',
        'current_password_failed'       =>      'Huidig wachtwoord is onjuist.',
        'no_change'                     =>      'Er is niks verandert aan uw instellingen.',
        'active_own_account'            =>      'Je kunt niet jouw eigen account uitschakelen in verband met de veiligheid van het systeem.',
        'active_administrator'          =>      'Je kunt geen administrator account uitzetten. Wil je dit toch? Verwijder dan eerst de administrator rechten.'
    ],
    
    'user' => [
        'added'                         =>      'Account is succesvol toegevoegd.',
        'deleted'                       =>      'Account is succesvol verwijdert.',
        'delete_admin'                  =>      'Let op! Het is niet mogelijk om een administrator te verwijderen.'
    ],

    'rights' => [
        'no_required_permissions'       =>      'Je hebt onvoldoende rechten voor deze actie.'
    ]
];
