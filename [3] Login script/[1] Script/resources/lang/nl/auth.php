<?php
return [
    'failed' => 'Deze gegevens zijn onjuist.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'account_not_active' => 'Jouw account is niet actief.'
];