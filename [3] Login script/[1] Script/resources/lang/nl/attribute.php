<?php

return [
    'user' => [
        'name'                  =>      'Gebruikersnaam',
        'email'                 =>      'Email',
        'firstname'             =>      'Voornaam',
        'lastname'              =>      'Achternaam',
        'rank'                  =>      'Functie',

        'password'              =>      'Wachtwoord',
        'password_confirmation' =>      'Wachtwoord herhaling',
        'current_password'      =>      'Huidig wachtwoord',

        'social_twitter'        =>      'Twitter',
        'social_facebook'       =>      'Facebook',
        'social_instagram'      =>      'Instagram',

        'remember'              =>      'Onthoud mij',
        'updated_at'            =>      'Aangepast op',
        'created_at'            =>      'Aangemaakt op'
    ]
];
