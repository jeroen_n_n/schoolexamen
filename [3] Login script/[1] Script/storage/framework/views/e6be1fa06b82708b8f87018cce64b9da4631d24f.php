<?php $__env->startSection('content'); ?>

<div class="row">
        <div class="col-12">

          
          
          
          <form method="POST" action="https://demo.businessmanager.app/profile" accept-charset="UTF-8" enctype="multipart/form-data" _lpchecked="1"><input name="_token" type="hidden" value="Spe2Cmq0w5OvdlgihmGrKJmi5qFUte8oCCmKDzjh">



          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Profile</h3>
            </div>
            <div class="card-body p-0 px-3">

              <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item pl-5">
                  <a class="nav-link active show" id="account-tab" data-toggle="tab" href="#account" role="tab" aria-selected="true">Account</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-selected="false">Contact details</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="localization-tab" data-toggle="tab" href="#localization" role="tab" aria-selected="false">Localization</a>
                </li>
              </ul>

              <div class="tab-content">
                <div class="tab-pane px-3 pb-3 active show" id="account" role="tabpanel" aria-labelledby="account-tab">
                  <div class="row">
                    <div class="col-md-6 col-lg-5">
                      <legend class="pt-5 pb-3" id="header1">Account</legend>

    


    		        	        	<div class="form-group">
    
    <label for="name" class="control-label required">Full name</label>
    <input class="form-control" required="required" minlength="2" maxlength="32" name="name" type="text" value="System Owner" id="name" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: pointer;">
    


        </div>
    		        	        	<div class="form-group">
    
    <label for="email" class="control-label required">Email address</label>
    <input class="form-control" required="required" name="email" type="email" value="info@example.com" id="email" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
    


        </div>
    		        	        	<div class="form-group">
    
    <label for="password" class="control-label">Password</label>
    <div class="row gutters-xs">
      <div class="col">
        <div class="input-icon">
          <span class="input-icon-addon">
            <i class="material-icons" style="font-size:16px">lock</i>
          </span>
          <input id="password" name="password" class="form-control" type="password" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;" autocomplete="off">        </div>
      </div>
      <span class="col-auto">
        <button id="toggle_password" class="btn btn-secondary" type="button" data-toggle="tooltip" title="" data-original-title="Show password"><i class="fe fe-eye"></i></button>
      </span>
      <span class="col-auto">
        <button onclick="$('#password').val(randomString(8));" class="btn btn-secondary" type="button" data-toggle="tooltip" title="" data-original-title="Generate password"><i class="fe fe-refresh-cw"></i></button>
      </span>
    </div>
        <small class="text-muted mt-2 float-left">
        Leave empty if you don't want to update the password.    </small>
<script>
$('#toggle_password').on('click', function() {
  if(! $(this).hasClass('active')) {
    $(this).addClass('active');
    $(this).find('i').removeClass('fe-eye').addClass('fe-eye-off');
    $(this).attr('data-original-title', "Hide password").tooltip('show');
    togglePasswordField('password', 'form-control', true);
  } else {
    $(this).removeClass('active');
    $(this).find('i').removeClass('fe-eye-off').addClass('fe-eye');
    $(this).attr('data-original-title', "Show password").tooltip('show');
    togglePasswordField('password', 'form-control', false);
  }
});
</script>



        </div>
    		    

                    </div>
                    <div class="col-md-6 col-lg-5 offset-lg-1">
                      <legend class="pt-5 pb-3" id="header2">Image</legend>

    


    		        	        	<div class="form-group">
        <label for="avatar" class="control-label">Avatar</label>
    <div class="row gutters-xs">
      <div class="col">
        <div class="custom-file">
        <input class="custom-file-input" accept="image/png, image/jpeg" onchange="setImageAvatar(this)" name="avatar" type="file" value="" id="avatar">          <label class="custom-file-label text-muted text-truncate" id="imageLabelAvatar">Select image...</label>
        </div>
      </div>
      <div class="col-auto">
        <button type="button" class="btn btn-secondary" onclick="deleteImageAvatar()" data-toggle="tooltip" title="" data-original-title="Remove image"><i class="fe fe-trash-2"></i></button>
      </div>
    </div>

    <input type="hidden" name="avatar_changed" id="avatar_changed" value="1">

    <img id="imagePreviewAvatar" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" class="my-5 avatar d-none" style="width:142px;height:142px;">

    
<script>
function setImageAvatar(_this) {
  $('#avatar_changed').val(1);
  var filePath = $(_this).val();
  var fileName = filePath.replace(/^.*\\/, "");

  var target = $(_this).next('.custom-file-label').attr('data-target');

  if (filePath != '') {
    $('#imageLabelAvatar').removeClass('text-muted').html(fileName);
  } else {
    deleteImageAvatar();
  }

  if (_this.files && _this.files[0]) {
    var target = $(_this).attr('id');
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#imagePreviewAvatar').removeClass('d-none');
      $('#imagePreviewAvatar').attr('src', e.target.result);
    }

    reader.readAsDataURL(_this.files[0]);
  }
}

function deleteImageAvatar() {
  $('#avatar_changed').val(1);
  $('#avatar').val("");
  $('#imageLabelAvatar').addClass('text-muted').html("Select image...");
  $('#imagePreviewAvatar').addClass('d-none').attr('src', 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7');
}
</script>



        </div>
    		    

                    </div>
                  </div>
                </div>
                <div class="tab-pane px-3 pb-3" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                  <div class="row">
                    <div class="col-md-6 col-lg-4">
                      <legend class="pt-5 pb-3" id="header3">Personal</legend>

    


    		        	        	<div class="form-group">
    
    <label for="salutation" class="control-label">Salutation</label>
    <input class="form-control" placeholder="Mr. / Ms." minlength="1" maxlength="32" name="salutation" type="text" id="salutation" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
    


        </div>
    		        	        	<div class="form-group">
    
    <label for="first_name" class="control-label">First name</label>
    <input class="form-control" minlength="1" maxlength="64" name="first_name" type="text" id="first_name" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
    


        </div>
    		        	        	<div class="form-group">
    
    <label for="last_name" class="control-label">Last name</label>
    <input class="form-control" minlength="1" maxlength="64" name="last_name" type="text" id="last_name" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
    


        </div>
    		        	        	<div class="form-group">
    
    <label for="job_title" class="control-label">Job title</label>
    <input class="form-control" minlength="1" maxlength="64" name="job_title" type="text" id="job_title" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
    


        </div>
    		        	        	<div class="form-group">
    
    <label for="date_of_birth" class="control-label">Date of birth</label>

    <div class="input-icon">
      <span class="input-icon-addon">
        <i class="material-icons" style="font-size:16px">calendar_today</i>
      </span>
      <input class="form-control" readonly="1" style="background-color: #fff" name="date_of_birth_field" type="text" value="">    </div>

    <input type="hidden" name="date_of_birth" id="date_of_birth" value="">

    <script>
$(function() {
  $('[name=date_of_birth_field]').datepicker({
    todayHighlight: false,
    clearBtn: true,
    format: {
      toDisplay: function (date, format, language) {
        $('#date_of_birth').val(moment(date).format('YYYY-MM-DD'));
        return moment(date).format('MMM Do YYYY');
      },
      toValue: function (date, format, language) {
        var d = new Date($('#date_of_birth').val());
        return new Date(d);
      }
    }
  }).on('changeDate', function(e) {
    if (e.dates.length === 0) {
      $('#date_of_birth').val('');
    }
  });

});
</script>



        </div>
    		    

                    </div>
                    <div class="col-md-6 col-lg-4">
                      <legend class="pt-5 pb-3" id="header4">Contact</legend>

    


    		        	        	<div class="form-group">
    
    <label for="phone" class="control-label">Phone</label>
    <input class="form-control" minlength="1" maxlength="32" name="phone" type="text" id="phone" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
    


        </div>
    		        	        	<div class="form-group">
    
    <label for="mobile" class="control-label">Mobile</label>
    <input class="form-control" minlength="1" maxlength="32" name="mobile" type="text" id="mobile" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
    


        </div>
    		        	        	<div class="form-group">
    
    <label for="website" class="control-label">Website</label>
    <input class="form-control" placeholder="https://" minlength="1" maxlength="250" name="website" type="text" id="website" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: pointer;">
    


        </div>
    		        	        	<div class="form-group">
    
    <label for="fax" class="control-label">Fax</label>
    <input class="form-control" minlength="1" maxlength="32" name="fax" type="text" id="fax" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
    


        </div>
    		    

                    </div>
                    <div class="col-md-6 col-lg-4">
                      <legend class="pt-5 pb-3" id="header5">Address</legend>

    


    		        	        	<div class="form-group">
    
    <label for="street1" class="control-label">Street</label>
    <input class="form-control" minlength="1" maxlength="250" name="street1" type="text" id="street1" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
    


        </div>
    		        	        	<div class="form-group">
    
    <label for="postal_code" class="control-label">Postal code / zip</label>
    <input class="form-control" minlength="1" maxlength="32" name="postal_code" type="text" id="postal_code" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
    


        </div>
    		        	        	<div class="form-group">
    
    <label for="city" class="control-label">City</label>
    <input class="form-control" minlength="1" maxlength="64" name="city" type="text" id="city" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
    


        </div>
    		        	        	<div class="form-group">
    
    <label for="state" class="control-label">State / province</label>
    <input class="form-control" minlength="1" maxlength="64" name="state" type="text" id="state" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
    


        </div>
    		        	        	<div class="form-group">
    
    <label for="country_code-selectized" class="control-label">Country</label>
        <select class="form-control selectize selectized" id="country_code" name="country_code" tabindex="-1" style="display: none;"><option value="" selected="selected"></option></select><div class="selectize-control form-control selectize single"><div class="selectize-input items not-full has-options"><input type="select-one" autocomplete="off" tabindex="" id="country_code-selectized" placeholder=" " style="width: 7.71875px;"></div><div class="selectize-dropdown single form-control selectize" style="display: none;"><div class="selectize-dropdown-content"></div></div></div>    


        </div>
    		    

                    </div>
                  </div>
                </div>
                <div class="tab-pane px-3 pb-3" id="localization" role="tabpanel" aria-labelledby="localization-tab">
                  <div class="row">
                    <div class="col-md-6 col-lg-5">
                      <legend class="pt-5 pb-3" id="header6">Language</legend>

    


    		        	        	<div class="form-group">
    
    <label for="language-selectized" class="control-label required">System language</label>
        <select class="form-control selectize selectized" required="" id="language" name="language" tabindex="-1" style="display: none;"><option value="en" selected="selected">English</option></select><div class="selectize-control form-control selectize single"><div class="selectize-input items required full has-options has-items"><div class="item" data-value="en">English</div><input type="select-one" autocomplete="off" tabindex="" id="language-selectized" style="width: 4px;"></div><div class="selectize-dropdown single form-control selectize" style="display: none;"><div class="selectize-dropdown-content"></div></div></div>    


        </div>
    		        	        	<legend class="pt-5 pb-3" id="header7">Date &amp; time</legend>

    


    		        	        	<div class="form-group">
    
    <label for="date_format-selectized" class="control-label required">Date format</label>
        <select class="form-control selectize selectized" required="" id="date_format" name="date_format" tabindex="-1" style="display: none;"><option value="m/d/Y" selected="selected">MM/DD/YYYY (05/23/2019)</option></select><div class="selectize-control form-control selectize single"><div class="selectize-input items required full has-options has-items"><div class="item" data-value="m/d/Y">MM/DD/YYYY (05/23/2019)</div><input type="select-one" autocomplete="off" tabindex="" id="date_format-selectized" style="width: 4px;"></div><div class="selectize-dropdown single form-control selectize" style="display: none;"><div class="selectize-dropdown-content"></div></div></div>    


        </div>
    		        	        	<div class="form-group">
    
    <label for="timezone-selectized" class="control-label required">Timezone</label>
        <select class="form-control selectize selectized" required="" id="timezone" name="timezone" tabindex="-1" style="display: none;"><option value="America/New_York" selected="selected">(GMT-4:00) Eastern Time (US and Canada)</option></select><div class="selectize-control form-control selectize single"><div class="selectize-input items required full has-options has-items"><div class="item" data-value="America/New_York">(GMT-4:00) Eastern Time (US and Canada)</div><input type="select-one" autocomplete="off" tabindex="" id="timezone-selectized" style="width: 4px;"></div><div class="selectize-dropdown single form-control selectize" style="display: none;"><div class="selectize-dropdown-content"></div></div></div>    


        </div>
    		        	        	<div class="form-group">
    
    <label for="time_format-selectized" class="control-label required">Time format</label>
        <select class="form-control selectize selectized" required="" id="time_format" name="time_format" tabindex="-1" style="display: none;"><option value="g:i a" selected="selected">12-hour clock (6:04 pm)</option></select><div class="selectize-control form-control selectize single"><div class="selectize-input items required full has-options has-items"><div class="item" data-value="g:i a">12-hour clock (6:04 pm)</div><input type="select-one" autocomplete="off" tabindex="" id="time_format-selectized" style="width: 4px;"></div><div class="selectize-dropdown single form-control selectize" style="display: none;"><div class="selectize-dropdown-content"></div></div></div>    


        </div>
    		    

                    </div>
                    <div class="col-md-6 col-lg-5 offset-lg-1">
                      <legend class="pt-5 pb-3" id="header8">Money</legend>

    


    		        	        	<div class="form-group">
    
    <label for="currency_code-selectized" class="control-label required">Currency</label>
        <select class="form-control selectize selectized" required="" id="currency_code" name="currency_code" tabindex="-1" style="display: none;"><option value="USD" selected="selected">US Dollar (USD)</option></select><div class="selectize-control form-control selectize single"><div class="selectize-input items required full has-options has-items"><div class="item" data-value="USD">US Dollar (USD)</div><input type="select-one" autocomplete="off" tabindex="" id="currency_code-selectized" style="width: 4px;"></div><div class="selectize-dropdown single form-control selectize" style="display: none;"><div class="selectize-dropdown-content"></div></div></div>    


        </div>
    		        	        	<legend class="pt-5 pb-3" id="header9">Numbers</legend>

    


    		        	        	<div class="form-group">
    
    <label for="seperators-selectized" class="control-label required">Notation</label>
        <select class="form-control selectize selectized" required="" id="seperators" name="seperators" tabindex="-1" style="display: none;"><option value=".," selected="selected">10,000,000.00</option></select><div class="selectize-control form-control selectize single"><div class="selectize-input items required full has-options has-items"><div class="item" data-value=".,">10,000,000.00</div><input type="select-one" autocomplete="off" tabindex="" id="seperators-selectized" style="width: 4px;"></div><div class="selectize-dropdown single form-control selectize" style="display: none;"><div class="selectize-dropdown-content"></div></div></div>    


        </div>
    		    

                    </div>
                  </div>
                </div>
              </div>

            </div>

            <div class="card-footer text-right">
                <button class="btn btn-primary" type="submit" id="submit_form_with_tabs">Update profile</button>
		    

            </div>
          </div>

          </form>

        </div>
      </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('parts.header', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>