<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name')); ?> - <?php echo $__env->yieldContent('pageTitle'); ?></title>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


    <script src="<?php echo e(asset('js/require.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/dashboard.js')); ?>"></script>
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">

    <script>
        requirejs.config({
            baseUrl: '././'
        });
    </script>
    
    <link href="<?php echo e(asset('plugins/charts-c3/plugin.css')); ?>" rel="stylesheet" />
    <script src="<?php echo e(asset('plugins/charts-c3/plugin.js')); ?>"></script>

    <link href="<?php echo e(asset('plugins/maps-google/plugin.css')); ?>" rel="stylesheet" />
    <script src="<?php echo e(asset('plugins/maps-google/plugin.js')); ?>"></script>

    <script src="<?php echo e(asset('plugins/input-mask/plugin.js')); ?>"></script>
</head>
<body>
    <div class="page">
        <div class="page-main">
            <div class="header py-4">
                <div class="container">
                    <div class="d-flex">
                        <a class="header-brand" href="<?php echo e(url('/')); ?>/admin/dashboard">
                            <b>Emp</b>rise
                        </a>
                        
                        
                        <div class="d-flex order-lg-2 ml-auto">
                            <div class="dropdown-divider"></div>
                            
                             <div class="dropdown">
                                <a href="javascript:void(0);" class="nav-link pr-0 leading-none" data-toggle="dropdown" data-target="#">
                                    <span class="avatar avatar-blue">
                                        <?php echo e(mb_substr(Auth::User()->firstname, 0, 1, "UTF-8") .  mb_substr(Auth::User()->lastname, 0, 1, "UTF-8")); ?>

                                    </span>
                                    
                                    <span class="ml-2 d-none d-lg-block">
                                        <span class="text-default"><?php echo e(Auth::User()->firstname); ?> <?php echo e(Auth::User()->lastname); ?></span>
                                        <small class="text-muted d-block mt-1"><?php echo e(Auth::User()->name); ?></small>
                                    </span>
                                </a>
                            </div>
                        </div>
                        
                        <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
                            <span class="header-toggler-icon"></span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg order-lg-first">
                            <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                                <li class="nav-item">
                                    <a href="<?php echo e(route('dashboard')); ?>" class="nav-link"><i class="fe fe-home"></i> Home</a>
                                </li>

                                <li class="nav-item">
                                    <a href="<?php echo e(route('settings.index')); ?>" class="nav-link"><i class="fe fe-settings"></i> Instellingen</a>
                                </li>

                                <li class="nav-item">
                                    <a href="<?php echo e(route('user.index')); ?>" class="nav-link"><i class="fe fe-settings"></i> Gebruikers</a>
                                </li>

                                <li class="nav-item">
                                    <a href="<?php echo e(route('logout')); ?>" class="nav-link"><i class="fe fe-lock"></i> Uitloggen</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="my-3 my-md-5">
                <div class="container">
                    <?php echo $__env->yieldContent('content'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
                