<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col">
            <?php if($errors->first()): ?>
                <div class="alert alert-icon alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert"></button>
                    <i class="fe fe-alert-triangle mr-2" aria-hidden="true"></i> <?php echo e($errors->first()); ?>

                </div>
            <?php endif; ?>

            <?php if(Session::has('success')): ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert"></button>
                    <?php echo e(Session::get('success')); ?>

                </div>
            <?php endif; ?>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Overzicht gebruikers (<?php echo e($users->total()); ?>)</h3>

                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('create.user')): ?>
                        <div class="card-options">
                            <a href="<?php echo e(route('user.create')); ?>" class="btn btn-secondary"><i class="fe fe-plus-circle"></i> Maak een account</a>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="table-responsive">
                    <table class="table table-hover table-outline table-vcenter text-nowrap card-table" width="100%">
                        <thead>
                            <tr>
                                <th class="text-center w-1"></th>
                                <th><?php echo e(trans('attribute.user.name')); ?></th>
                                <th><?php echo e(trans('attribute.user.email')); ?></th>
                                <th ><?php echo e(trans('attribute.user.rank')); ?></th>
                                <th class="text-center" width="60"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td class="text-center">
                                         <span class="avatar <?php if($user->active): ?> avatar-green <?php else: ?> avatar-red <?php endif; ?>">
                                            <?php echo e(mb_substr($user->firstname, 0, 1, "UTF-8") .  mb_substr($user->lastname, 0, 1, "UTF-8")); ?>

                                        </span>
                                    </td>
                                    
                                    <td>
                                        <div><?php echo e($user->name); ?></div>
                                        <div class="small text-muted">
                                            <?php echo e($user->firstname); ?> <?php echo e($user->lastname); ?>

                                        </div>
                                    </td>

                                    <td>
                                        <?php echo e($user->email); ?>

                                    </td>
                                    
                                    <td>
                                        <?php echo e(ucfirst( isset($user->roles[0]) ? $user->roles[0]->name : 'User')); ?>

                                    </td>
                                    
                                    <td>                                      
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit.user')): ?>
                                            <a href="<?php echo e(route('user.edit', $user->id)); ?>" class="btn btn-secondary"><i class="fe fe-edit-2" data-toggle="tooltip" data-original-title="Bewerk profiel"></i></a>
                                        <?php endif; ?>

                                        <a href="<?php echo e(route('user.show', $user->id)); ?>" class="btn btn-primary"><i class="fe fe-user" data-toggle="tooltip" data-original-title="Bekijk gebruiker"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>

                <div class="modal-footer">
                    <?php echo e($users->links()); ?>

                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('parts.header', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>