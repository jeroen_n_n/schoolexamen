<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name')); ?></title>

    <script src="<?php echo e(asset('js/app.js')); ?>" defer></script>
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
</head>

<body>
    <div class="page">
        <div class="page-single">
            <div class="container">
                <div class="row">
                    <div class="col col-login mx-auto">
                    
                        <?php if($errors->first()): ?>
                            <div class="alert alert-icon alert-danger" role="alert">
                                <i class="fe fe-alert-triangle mr-2" aria-hidden="true"></i> <?php echo e($errors->first()); ?>

                            </div>
                        <?php endif; ?>

                        <form class="card" method="post">
                            <?php echo csrf_field(); ?>

                            <div class="card-body p-6">
                                <div class="card-title"><?php echo app('translator')->getFromJson('auth.title'); ?></div>
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" placeholder="<?php echo app('translator')->getFromJson('auth.name'); ?>" autocomplete="off" required>
                                </div>
                                
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" placeholder="<?php echo app('translator')->getFromJson('auth.password'); ?>" autocomplete="off" required>
                                </div>
                                
                                <div class="form-group">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="remember_token" class="custom-control-input" />
                                        <span class="custom-control-label"><?php echo app('translator')->getFromJson('auth.remember_me'); ?></span>
                                    </label>
                                </div>

                                <div class="form-footer">
                                    <button type="submit" class="btn btn-primary btn-block"><?php echo app('translator')->getFromJson('auth.submit'); ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>                          
</body>
</html>