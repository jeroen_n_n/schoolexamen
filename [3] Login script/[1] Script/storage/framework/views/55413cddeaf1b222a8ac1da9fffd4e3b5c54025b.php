<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col">
            <?php if($errors->first()): ?>
                <div class="alert alert-icon alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert"></button>
                    <i class="fe fe-alert-triangle mr-2" aria-hidden="true"></i> <?php echo e($errors->first()); ?>

                </div>
            <?php endif; ?>

            <?php if(Session::has('success')): ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert"></button>
                    <?php echo e(Session::get('success')); ?>

                </div>
            <?php endif; ?>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Bewerk gebruiken</h3>
                </div>

                <div class="card-body p-0 px-3">
                    <form action="<?php echo e(route('user.update', [Auth::Id()])); ?>" method="post">
                        <?php echo method_field('put'); ?>
                        <?php echo csrf_field(); ?>
                            
                        <div class="tab-content">
                            <div class="tab-pane px-3 pb-3 active show" id="account" role="tabpanel" aria-labelledby="account-tab">
                                <div class="row">
                                    <div class="col-4">
                                        <legend class="pt-5 pb-3">Account</legend>
                                        
                                        <div class="form-group">
                                            <label class="control-label"><?php echo e(trans('attribute.user.name')); ?></label>
                                            <input type="text" class="form-control <?php if($errors->has('name')): ?> is-invalid <?php endif; ?>" minlength="2" maxlength="32" value="<?php echo e(Auth::User()->name); ?>" disabled>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label required"><?php echo e(trans('attribute.user.email')); ?></label>
                                            <input type="email" name="email" class="form-control <?php if($errors->has('email')): ?> is-invalid <?php endif; ?>" minlength="2" maxlength="32" autocomplete="off" value="<?php echo e(Auth::User()->email); ?>" required>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label required"><?php echo e(trans('attribute.user.firstname')); ?></label>
                                            <input type="text" name="firstname" class="form-control <?php if($errors->has('firstname')): ?> is-invalid <?php endif; ?>" minlength="2" maxlength="32" autocomplete="off" value="<?php echo e(Auth::User()->firstname); ?>" required>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label required"><?php echo e(trans('attribute.user.lastname')); ?></label>
                                            <input type="text" name="lastname" class="form-control <?php if($errors->has('lastname')): ?> is-invalid <?php endif; ?>" minlength="2" maxlength="32" autocomplete="off" value="<?php echo e(Auth::User()->lastname); ?>" required>
                                        </div>
                                    </div>
                                    
                                    <div class="col-4">
                                        <legend class="pt-5 pb-3">Wachtwoord</legend>

                                        <div class="form-group">
                                            <label class="form-label"><?php echo e(trans('attribute.user.password')); ?></label>
                                            <input type="password" name="password" class="form-control <?php if($errors->has('password')): ?> is-invalid <?php endif; ?>" minlength="2" maxlength="32" autocomplete="off"/>
                                        </div>
            
                                        <div class="form-group">
                                            <label class="form-label"><?php echo e(trans('attribute.user.password_confirmation')); ?></label>
                                            <input type="password" name="password_confirmation" class="form-control <?php if($errors->has('password_confirmation')): ?> is-invalid <?php endif; ?>"  minlength="2" maxlength="32" autocomplete="off"/>
                                        </div>
                                    </div>

                                    <div class="col-4">
                                        <legend class="pt-5 pb-3">Social media</legend>
                                                    
                                        <div class="form-group">
                                            <label class="control-label"><?php echo e(trans('attribute.user.social_twitter')); ?></label>
                                            <input type="text" name="social_twitter" class="form-control <?php if($errors->has('social_twitter')): ?> is-invalid <?php endif; ?>" minlength="1" maxlength="32" placeholder="https://twitter.com/" autocomplete="off" required>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label"><?php echo e(trans('attribute.user.social_facebook')); ?></label>
                                            <input type="text" name="social_facebook" class="form-control <?php if($errors->has('social_facebook')): ?> is-invalid <?php endif; ?>" minlength="1" maxlength="32" placeholder="https://facebook.com" autocomplete="off" required>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label"><?php echo e(trans('attribute.user.social_instagram')); ?></label>
                                            <input type="text" name="social_instagram" class="form-control <?php if($errors->has('social_instagram')): ?> is-invalid <?php endif; ?>" minlength="1" maxlength="32" placeholder="https://instagram.com" autocomplete="off" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-footer text-right">
                                    <button class="btn btn-primary" type="submit">Update account</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('parts.header', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>