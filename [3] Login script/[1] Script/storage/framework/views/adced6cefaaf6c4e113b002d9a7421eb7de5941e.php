<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col">
            <?php if($errors->first()): ?>
                <div class="alert alert-icon alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert"></button>
                    <i class="fe fe-alert-triangle mr-2" aria-hidden="true"></i> <?php echo e($errors->first()); ?>

                </div>
            <?php endif; ?>

            <?php if(Session::has('success')): ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert"></button>
                    <?php echo e(Session::get('success')); ?>

                </div>
            <?php endif; ?>
        </div>
    </div>


    <?php if(auth()->check() && auth()->user()->hasRole('administrator')): ?>
    I am a super-admin!
<?php else: ?>
    I am not a super-admin...
<?php endif; ?>   
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"><?php echo e(trans('user.title.overview')); ?> (<?php echo e($users->total()); ?>)</h3>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-striped table-vcenter">
                        <thead>
                            <tr>
                                <th width="120"><?php echo e(trans('user.attribute.id')); ?></th>
                                <th width="100"><?php echo e(trans('user.attribute.name')); ?></th>
                                <th width="100"><?php echo e(trans('user.attribute.active')); ?></th>
                                <th width="5"><?php echo e(trans('user.attribute.rank')); ?></th>
                                <th width="100"></th>
                                <th width="100"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <tr>
                                    <td><?php echo e($user->id); ?></td>
                                    <td><?php echo e($user->name); ?></td>
                                    <td><?php echo e($user->active); ?></td>
                                    
                                    <td><button class="btn btn-info">Zet op non-actief</button></td>
                                    <td><button class="btn btn-danger">Verwijder gebruiker</button></td>
                                </tr>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>

                <div class="modal-footer">
                    <?php echo e($users->links()); ?>

                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('parts.header', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>