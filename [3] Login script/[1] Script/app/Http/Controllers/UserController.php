<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\StoreUserRequest;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Mail;
use App\Mail\DeleteUser;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\User;
use Auth;
use Session;
use Hash;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('id')->paginate(8);
        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $request = $request->except('_method', '_token', 'password_confirmation');
            
        if(array_key_exists('password', $request)) {
            $request['password'] = Hash::make($request['password']);
        }

        $id = DB::table('users')->insertGetId($request);
        $search = User::FindOrFail($id);
        $search->assignRole('user');

        Session::flash('success', trans('message.user.added'));

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $user = User::findOrFail($id);
        $permissions = $user->getAllPermissions();
        
        return view('user.show', compact('user', 'permissions'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $user = User::findOrFail($id);
        return view('user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $errors = new \Illuminate\Support\MessageBag();

        // Check if the user can edit another user;
        if(!Auth::User()->can('edit.user')) {
            $errors->add(null, trans('message.rights.no_required_permissions'));
        }

        // If the password is changed, check if the user has the right permission;
        else if($request->password && !Auth::User()->can('edit.password.user')) {
            $errors->add(null, trans('message.rights.no_required_permissions'));
        }

        else {
            // Take all the used requests except the default laravel ones and the password confirmation;
            $request = $request->except('_method', '_token', 'password_confirmation');
            
            // If password exits, create a new hash and override the existing password request;
            if(array_key_exists('password', $request)) {
                $request['password'] = Hash::make($request['password']);
            }

            // Update users;
            DB::table('users')->where('id', $id)->update($request);
            Session::flash('success', trans('message.settings.success'));
        }

        // Return back with errors if needed;
        return back()->withErrors($errors);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(int $id, bool $status)
    {
        $errors = new \Illuminate\Support\MessageBag();

        // Check if the user can edit another user;
        if(!Auth::User()->can('edit.user') || !Auth::User()->can('activate.user')) {
            $errors->add(null, trans('message.rights.no_required_permissions'));
        }

        // Its not possible to change the active-status of your own account;
        // This is for security-reasons;
        else if(Auth::Id() === $id) {
            $errors->add(null, trans('message.settings.active_own_account'));
        }
        
        // Whoo, there are no errros;
        else {            
            DB::table('users')->where('id', $id)->update(['active' => !$status]);
            Session::flash('success', trans('message.settings.success'));
        }

        // Return back with errors if needed;
        return back()->withErrors($errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::FindOrFail($id);
        $errors = new \Illuminate\Support\MessageBag();
        
        // Check if the user has the permission to delete the user;
        if(!Auth::User()->can('delete.user')) {
            $errors->add(null, trans('message.rights.no_required_permissions'));
        }
        
        // Check if the user is an administrator - if so THIS IS NOT POSSIBLE!;
        elseif($user->hasrole('administrator')) {            
            $errors->add(null, trans('message.user.delete_admin'));
        }

        else {
            $user->delete();

            Session::flash('success', trans('message.user.deleted'));
            return redirect()->route('user.index');
        }

        return back()->withErrors($errors);
    }
}
