<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index ()
    {
        $administratorsCount = User::role('administrator')->get()->count();
        $usersCount = User::role('user')->get()->count();

        $users = User::All()->count();
        $usersNotActive = User::All()->where('active', 0)->count();

        return view('dashboard.index', compact('administratorsCount', 'usersCount', 'users', 'usersNotActive'));
    }
}
