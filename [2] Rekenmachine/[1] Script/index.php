<?php
// Check if the server request is POST;
// We need an new POST request;
if($_SERVER['REQUEST_METHOD'] === 'POST') {

    // Check if the calculator is used by checking the post;
    // The function 'isset()' is not good enough;
    // If the user is using 0 as som, isset will see that as 'false' but there is an input;
    if(isset($_POST['calculate'])) {

        // Extra test to make sure there is an input;
        if($_POST['calculate'] != '') {

            // Lets create an variable so its fast accesable;
            // Input is filterd because i never trust the input of the user;
            $calculate = htmlspecialchars($_POST['calculate']);

            // Remove letters incase the user is trying to bypass the POST;
            $input = preg_replace('/[^A-Za-z0-9.+:*\/-]/', null, $calculate);
            $input = str_replace(':', '/', $input);
            $input = preg_replace('/\.( |$)/', '\1', $input);

            // We have to make an split for all the operators (components);
            // Allowed: * / + - .
            $components = preg_split('~([*/+-.])~', $input, NULL, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
            
            // Create an array with the allowed operators;
            $operators = ['*', '/', '+', '-', '.'];
            
            // CHeck if input is valid;
            $valid = true;

            // The best way to describe whats happing is this:
            // This part of the script makes sure that there won't be an double operator;
            // Example: 4++3 = no valid som;
            // The scripts finds the + and checks if there is another operator next to the +;
            // If this is the case, there is NO valid som and the user doesn't get an value output;

            // Loop all the operators
            foreach($operators as $operator) {

                // Search the operators in the components;
                if($index = array_search($operator, $components)) {

                    // Loop the components;
                    foreach($components as $i => $value) {

                        // Only take the operators out of the components;
                        if($value === $operator) {

                            // Check if an higher key exists in the array;
                            if(array_key_exists($i, $components) && array_key_exists($i-1, $components)) {
                                
                                // Check if the array its self exists;
                                if(in_array($components[$i], $operators) && in_array($components[$i-1], $operators)) {
                                    $valid = false;
                                }
                            }

                            // Check if an lower key exists in the array;
                            else if(array_key_exists($i, $components) && array_key_exists($i+1, $components)) {
                                
                                // Check if the array its self exists;
                                if(in_array($components[$i], $operators) && in_array($components[$i+1], $operators)) {
                                    $valid = false;                                 
                                }
                            }
                        }
                    }

                    // Check if there is an value before and after the $operator split;
                    if(array_key_exists($index-1, $components) && array_key_exists($index+1, $components) && $valid) {
                
                        // Check if the value is num (before and after the $operator
                        if(is_numeric($components[$index-1]) && is_numeric($components[$index+1])) {
                            
                            // Eval will be used to make the som simple to use;
                            // This function will translate .. + .. (example: 4+4) in an direct som and will calculate this;
                            eval('$output = '. $input .';');
                        }
                    }
                }
            }
        }
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Examen</title>
    <link href="assets/css/layout.css" rel="stylesheet">
</head>
<body>
    <div id="calculator">
        <div id="message">
            ~ Jeroen Nijssen - Examen rekenmachine PHP ~
        </div>

        <div id="result">
            <form method="post" id="form">
                <input type="text" name="calculate" value="<?php echo (isset($output) && $output != '' ? $input : '0'); ?>" max="50" readonly="readonly">
            </form>

            <span><?php echo (isset($output) && $output != '' ? ' = ' . $output : '= ..'); ?></span>
        </div>

        <div class="nums">
            <button value="7">7</button>
            <button value="8">8</button>
            <button value="9">9</button>
            <button value="/">/</button>
                
            <button value="4">4</button>
            <button value="5">5</button>
            <button value="6">6</button>
            <button value="+">+</button>
 
            <button value="1">1</button>
            <button value="2">2</button>
            <button value="3">3</button>
            <button value="-">-</button>
             
            <button value="0">0</button>
            <button value=".">.</button>
            <button value="*">*</button>
            <button value="" class="submit">=</button>
        </div>

        <div class="clear">Leeg rekenmachine</div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="assets/js/calc.js"></script>
</body>
</html>