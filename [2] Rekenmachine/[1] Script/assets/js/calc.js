$(function() {	
	$(".nums button").click(function() {
		var value = $(this).attr("value");
		var target = $('#result input');

		if(target.val() === '0') 
			target.val('');
		
		target.val (
			target.val() + value
		);
	});

	$(".submit").click(function() {
		$('#form').submit();
	});

	$(".clear").click(function() {
		$('#result input').val('0');
	});
});
